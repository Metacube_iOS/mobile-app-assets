//
//  ListenerDispatcher.swift
//  TWGiOS
//
//  Created by Samiksha on 22/09/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

protocol ListenerDispatcherProtocol: class {
    func didFireEventWithDispatcher(_ dispatcher: ListenerDispatcher, withEvent event: AnyObject)
}

class ListenersDispatcherOptions {
    var isIgnoreThrottling = true
}

fileprivate class ListenerDispatcherHelper {
    var eventType: AnyClass!
    weak var listenerObject: ListenerDispatcherProtocol?
    weak var dispatcher: ListenerDispatcher?
    var options: ListenersDispatcherOptions?
    weak var queue: OperationQueue?
}

class ListenerDispatcher: NSObject {
    private var listenerTable = NSMapTable<AnyObject, AnyObject>()
    private var _defaultQueue: OperationQueue?
    private var throttleHelper: ListenerDispatcherThrottleHelper?
    
    static let shared = ListenerDispatcher()
    
    override init() {
        super.init()
        listenerTable = NSMapTable<AnyObject, AnyObject>.strongToStrongObjects()
        throttleHelper = ListenerDispatcherThrottleHelper.init(delegate: self)
    }
    
    func defaultQueue() -> OperationQueue {
        if _defaultQueue == nil {
            _defaultQueue = OperationQueue.init()
        }
        return _defaultQueue!
    }
    
    func addListener(_ listener: ListenerDispatcherProtocol, eventType: AnyClass) {
        addListener(listener, eventType: eventType, self.defaultQueue())
    }
    
    func addListener(_ listener: ListenerDispatcherProtocol, options: ListenersDispatcherOptions?, eventType: AnyClass) {
        addListener(listener, options, eventType: eventType, queue: self.defaultQueue())
    }
        
    func addListener(_ listener: ListenerDispatcherProtocol, eventType: AnyClass, _ queue: OperationQueue) {
        addListener(listener, nil, eventType: eventType, queue: queue)
    }
    
    func addListener(_ listener: ListenerDispatcherProtocol, _ options: ListenersDispatcherOptions?, eventType: AnyClass, queue: OperationQueue) {
        let helper: ListenerDispatcherHelper = ListenerDispatcherHelper.init()
        helper.dispatcher = self
        helper.listenerObject = listener
        helper.eventType = eventType
        helper.options = options
        helper.queue = queue
        
        var helperArray = [ListenerDispatcherHelper]()
        if listenerTable.object(forKey: listener as AnyObject) != nil {
            helperArray = listenerTable.object(forKey: listener as AnyObject) as! [ListenerDispatcherHelper]
        }
        
        var updatedHelperArray: [ListenerDispatcherHelper] = [ListenerDispatcherHelper]()
        if helperArray.count > 0 {
            updatedHelperArray = helperArray
        }
        updatedHelperArray.append(helper)
        listenerTable.setObject(updatedHelperArray as AnyObject, forKey: listener as AnyObject)
    }
    
    func removeListener(_ listener: ListenerDispatcherProtocol) {
        if (listenerTable.object(forKey: listener as AnyObject) != nil) {
            listenerTable.removeObject(forKey: listener as AnyObject)
        }
    }
    
    func fire(event: AnyObject) {
        let listenerEnumerator = listenerTable.objectEnumerator()
        for helperArray in (listenerEnumerator?.allObjects)! {
            if helperArray is [ListenerDispatcherHelper] {
                for dispatcherHelper in (helperArray as! [ListenerDispatcherHelper]) {
                    if type(of: event) == dispatcherHelper.eventType {
                        dispatcherHelper.listenerObject?.didFireEventWithDispatcher(self, withEvent: event )
                    }
                }
            }
        }
    }
    
    func fireEventNow(_ event: AnyObject, includeListenersIgnoringThrottling: Bool, includeListenersNotIgnoringThrottling: Bool) {
        let listenerEnumerator = listenerTable.objectEnumerator()

        for helperArray in (listenerEnumerator?.allObjects)! {
            if helperArray is [ListenerDispatcherHelper] {
                for dispatcherHelper in (helperArray as! [ListenerDispatcherHelper]) {
                    
                    if type(of: event) == dispatcherHelper.eventType {
                    
                        let ignoresThrottling: Bool = dispatcherHelper.options != nil ? (dispatcherHelper.options?.isIgnoreThrottling)! : true
                        
                        let shouldSend: Bool = ignoresThrottling ? includeListenersIgnoringThrottling : includeListenersNotIgnoringThrottling
                        
                        if shouldSend {
                            dispatcherHelper.queue?.addOperation({[unowned self] () -> Void in
                                dispatcherHelper.listenerObject?.didFireEventWithDispatcher(self, withEvent: event)
                            })
                        }
                    }
                }
            }
        }
    }
    
    func setThrottle(_ throttleValue: TimeInterval, forEventType eventType: AnyClass) {
        throttleHelper?.setThrottle(throttleValue, forEventType: eventType)
    }
    
    func throttle(forEventType eventType: AnyClass) -> TimeInterval {
        return throttleHelper != nil ? (throttleHelper?.throttle(forEventType: eventType))! : 0
    }
    
    func removeThrottle(forEventType eventType: AnyClass) {
        throttleHelper?.removeThrottle(forEventType: eventType)
    }
}

extension ListenerDispatcher: ListenersDispatcherThrottleHelperDelegate {
    func listenersDispatcherThrottleHelper(_ helper: ListenerDispatcherThrottleHelper, shouldDeliverEvent event: AnyObject) {
        self.fireEventNow(event, includeListenersIgnoringThrottling: false, includeListenersNotIgnoringThrottling: true)
    }
}
