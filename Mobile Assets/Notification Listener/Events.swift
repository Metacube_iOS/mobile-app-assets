//
//  Events.swift
//  TWGiOS
//
//  Created by Samiksha on 22/09/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

// MARK: Profile update events

class VendorProfileUpdateEvent: NSObject  {
    enum ProfileUpdateStatus {
        case approved, declined
    }
    
    var status: ProfileUpdateStatus!
}

//  MARK: Bid events

class BidReceivedEvent: NSObject {
    var bidId: String?
}

class BidUpdateEvent {
    var bidId: String?
}

class CancelBidEvent {
    var bidId: String?
}

class BidAcceptedEvent: NSObject {
    var requestId: String?
}

//  MARK: Service Request events

class DeleteRequestEvent {
    var requestId: String?
}

class RequestExpiredEvent: NSObject {
    var requestId: String?
}

class RequestExtendedEvent: NSObject {
    var requestId: String?
}

class RequestCancelledEvent: NSObject {
    var requestId: String?
}

class NewJobEvent: NSObject {
    var requestId: String?
}
