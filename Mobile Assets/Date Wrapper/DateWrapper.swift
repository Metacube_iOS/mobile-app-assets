//
//  DateWrapper.swift
//  TWGiOS
//
//  Created by Samiksha on 17/07/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation
import DateToolsSwift

enum PeriodOfTheDay {
    case morning, afternoon, evening
}

class DateWrapper {
    
    static func getPeriodOfTheDay() -> PeriodOfTheDay {
        let now: Date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: now)
        switch hour {
        case 0 ... 12:
            return .morning
        case 13 ... 14:
            return .afternoon
        default:
            return .evening
        }
    }
    
    static func interval(fromDate date: String) -> Int {
        guard date != "" else {
            return 0
        }
        
        let currentCalendar = Calendar.current
        let todaysDate = Date()
        let currentDateString = todaysDate.toString(inFormat: completeDateFormat)
        let givenDate = date.toDate(fromFormat: completeDateFormat)
        let currentDate = currentDateString?.toDate(fromFormat: completeDateFormat)
        
        if currentCalendar.isDateInToday(givenDate!) {
            return 0
        } else if currentCalendar.isDateInYesterday(givenDate!) {
            return 1
        } else {
            let components = currentCalendar.dateComponents([.day], from: givenDate!, to: currentDate!)
            return components.day!
        }
    }
    
    static func timeAgo(fromDate date: Date) -> String {
        let yearsAgo = date.yearsAgo
        let monthsAgo = date.monthsAgo
        let weekAgo = date.weeksAgo
        let daysAgo = date.daysAgo
        let hoursAgo = date.hoursAgo
        let minutesAgo = date.minutesAgo
        
        if yearsAgo >= 1 {
            return yearsAgo > 1 ? String(yearsAgo) + " Years Ago" : String(yearsAgo) + " Year Ago"
        } else if monthsAgo >= 1 {
            return monthsAgo > 1 ? String(monthsAgo) + " Months Ago" : String(monthsAgo) + " Month Ago"
        } else if weekAgo >= 1 {
            return weekAgo > 1 ? String(weekAgo) + " Weeks Ago" : String(weekAgo) + " Week Ago"
        } else if daysAgo >= 1 {
            return String(daysAgo) + (daysAgo > 1 ? " Days Ago" : " Day Ago")
        } else if hoursAgo >= 1 {
            return String(hoursAgo) + (hoursAgo > 1 ? " Hours Ago" : "Hour Ago")
        } else if minutesAgo >= 1 {
            return String(minutesAgo) + (minutesAgo > 1 ? " Minutes Ago" : " Minute ago")
        } else {
            return LocalizableKeys.justNow.localizedString
        }
    }
    
    static func timeAgo(fromDateString dateString: String) -> String {
        let date = dateString.toDate(fromFormat: completeDateFormat)
        if date != nil {
            return self.timeAgo(fromDate: date!)
        }
        return ""
    }
}
