//
//  String+Date.swift
//  RxAppoint
//
//  Created by Ankita Porwal on 28/12/16.
//  Copyright © 2016 TechEversion. All rights reserved.
//

import Foundation

let yearFirstFormat = "yyyy-MM-dd"
let dayFirstFormat = "dd MMM yyyy"
let timeFormat = "hh:mm a"
let completeDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
let dateTimeFormat = "dd MMM yyyy hh:mm a"

extension Date {
    func toString(inFormat format: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    func toTime(inFormat format: String = timeFormat) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        return dateFormatter.string(from: self)
    }
    
    var millisecondsSince1970: Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}

extension String {
    func toDate(fromFormat format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
    }
}
