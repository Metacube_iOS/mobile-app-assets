//
//  ContentService.swift
//  TWGiOS
//
//  Created by Samiksha on 04/07/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

protocol ContentService {
    
    static func termsAndConditionsLink() -> String
    static func requestAccpetedTermsAndConditionsLink() -> String
    static func authCode() -> String
    
}
