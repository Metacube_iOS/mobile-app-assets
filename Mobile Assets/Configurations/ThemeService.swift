//
//  ThemeService.swift
//  TWGiOS
//
//  Created by Ankita Porwal on 28/11/16.
//  Copyright © 2016 the warranty group. All rights reserved.
//

import UIKit

protocol ThemeService {
    
    //    MARK: color methods
    
    static func themeColor() -> UIColor
    static func grayColor() -> UIColor
    static func charcoalGrayColor() -> UIColor
    static func nonEditableFieldUnderlineColor() -> UIColor
    static func linkColor() -> UIColor
    static func textViewBorderColor() -> UIColor
    static func homeScreenIconsColor() -> UIColor
    static func emptyStateBackgroundColor() -> UIColor
    static func pageIndicatorBaseColor() -> UIColor
    static func whiteColor() -> UIColor
    static func clearColor() -> UIColor
    static func redColor() -> UIColor
    static func lightGrayColor() -> UIColor
    static func navBarGradientStop1Color() -> UIColor
    static func navBarGradientStop2Color() -> UIColor
    static func warmGrayColor() -> UIColor
    static func purpleyGrey() -> UIColor
    static func imageViewBorderColor() -> UIColor
    static func shadowColor() -> UIColor
    static func lightWarmGrayColor() -> UIColor
    static func websiteLinkColor() -> UIColor
    static func pickerBgColor() -> UIColor
    static func requestListCellShadowColor() -> UIColor
    static func requestListBackgroundColor() -> UIColor
    static func turquoiseBlue() -> UIColor
    static func whiteTranslucentColor() -> UIColor
    static func coralRedColor() -> UIColor
    static func algaeGreenColor() -> UIColor
    static func bulletBaseColor() -> UIColor
    static func blueColor() -> UIColor
    static func tabBarBorderColor() -> UIColor
    static func lightSageColor() -> UIColor
    static func coralOrangeColor() -> UIColor
    static func lightCoralColor() -> UIColor
    static func invoiceHeadingBackgroundColor() -> UIColor
    static func blackColor() -> UIColor
    static func scarletRedColor() -> UIColor
    static func darkSkyBlueColor() -> UIColor
    
    //    MARK: font methods
    
    static func avenirRomanFont(ofSize size: CGFloat) -> UIFont?
    static func avenirBookFont(ofSize size: CGFloat) -> UIFont?
    static func avenirHeavyFont(ofSize size: CGFloat) -> UIFont?
    static func avenirMediumFont(ofSize size: CGFloat) -> UIFont?
    static func avenirLightFont(ofSize size: CGFloat) -> UIFont?
    static func avenirBlackFont(ofSize size: CGFloat) -> UIFont?
    
    //    MARK: gradient methods
    
    static func gradientLayerForHorizontalBounds(bounds: CGRect) -> CAGradientLayer
    static func gradientLayerForVerticleBounds(bounds: CGRect) -> CAGradientLayer
    static func imageLayerForGradientBackground(bounds: CGRect) -> UIImage
}
