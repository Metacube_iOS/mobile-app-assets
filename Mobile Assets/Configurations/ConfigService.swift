//
//  ConfigService.swift
//  TWGiOS
//
//  Created by Samiksha on 04/07/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

protocol ConfigService {
    
    static func serverBaseURL() -> String
    static func unauthenticatedServerBaseURL() -> String
    static func remoteAccessConsumerKey() -> String
    static func oAuthRedirectURI() -> String
}
