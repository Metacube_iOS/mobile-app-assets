//
//  AppManager.swift
//  TWGiOS
//
//  Created by Samiksha on 04/07/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

private class AppLoader {
    
    static let configService = App.currentApp.configService
    static let contentService = App.currentApp.contentService
    static let themeService = App.currentApp.themeService
    static let mediaService = App.currentApp.mediaService
}

struct AppManager {
    
    static let baseUrl = AppLoader.configService.serverBaseURL()
    static let unauthenticatedAPIURL = AppLoader.configService.unauthenticatedServerBaseURL()
    static let remoteAccessConsumerKey = AppLoader.configService.remoteAccessConsumerKey()
    static let oAuthRedirectURI = AppLoader.configService.oAuthRedirectURI()
    static let authCode = AppLoader.contentService.authCode()
    static let termsAndConsitionsLink = AppLoader.contentService.termsAndConditionsLink()
    static let requestAccesptedTermsAndConditionsLink = AppLoader.contentService.requestAccpetedTermsAndConditionsLink()
    static let theme = AppLoader.themeService
    static let mediaService = AppLoader.mediaService
}
