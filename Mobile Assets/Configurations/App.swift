//
//  App.swift
//  TWGiOS
//
//  Created by Samiksha on 04/07/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

enum App: String {
    
    case NMI = "com.twg.twgios.TWGiOS2"
    
    static var currentApp: App {
        return App(rawValue: Bundle.main.bundleIdentifier!)!
    }
    
    var themeService: ThemeService.Type {
        switch self {
        case .NMI: return NMIThemeService.self
        default: preconditionFailure("App could not determine any bundle identifier.")
        }
    }
    
    var contentService: ContentService.Type {
        switch self {
        case .NMI: return NMIContentService.self
        default: preconditionFailure("App could not determine any bundle identifier.")
        }
    }
    
    var configService: ConfigService.Type {
        switch self {
        case .NMI: return NMIConfigService.self
        default: preconditionFailure("App could not determine any bundle identifier.")
        }
    }
    
    var mediaService: MediaService.Type {
        switch self {
        case .NMI: return NMIMediaService.self
        default:
            preconditionFailure("App could not determine any bundle identifier.")
        }
    }
    
}

