//
//  MediaService.swift
//  TWGiOS
//
//  Created by Ankita Porwal on 24/07/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

enum MediaStorageServiceType {
    case customerProfile, vendorProfile, productWallet, serviceRequest
}

protocol MediaService {
    static func mediaStoragePath(for service: MediaStorageServiceType) -> String
}
