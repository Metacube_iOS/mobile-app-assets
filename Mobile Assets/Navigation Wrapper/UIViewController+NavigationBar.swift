//
//  UIViewController+NavigationBar.swift
//  TWGiOS
//
//  Created by Ankita Porwal on 26/07/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

extension UINavigationController {
    
    enum NavigationBarType {
        case gradient, blue, white
    }
    
    func setNavigationBarAppearance(toType type: NavigationBarType) {
        switch type {
        case .gradient: navigationBar.setBackgroundImage(AppManager.theme.imageLayerForGradientBackground(bounds: (navigationBar.bounds)), for: .default)
            navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: AppManager.theme.whiteColor()]
            break
        case .blue:
            navigationBar.barTintColor = AppManager.theme.navBarGradientStop1Color()
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.shadowImage = UIImage()
            navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: AppManager.theme.whiteColor()]
            break
        case .white:
            navigationBar.barTintColor = AppManager.theme.whiteColor()
            navigationBar.tintColor = AppManager.theme.themeColor()
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.shadowImage = UIImage()
            navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: AppManager.theme.themeColor()]
            break
        }
    }
    
}
