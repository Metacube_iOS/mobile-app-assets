//
//  UIViewController+tBarButtons.swift
//  TWGiOS
//
//  Created by Ankita Porwal on 13/12/16.
//  Copyright © 2016 the warranty group. All rights reserved.
//

import UIKit

extension UIViewController {
    
    enum ButtonType {
        case white, blue
    }
    
    //    MARK: left bar button
    
    func setupBackButton() {
        setupBackButton(withType: .blue, withAction: #selector(UIViewController.backButtonTapped))
    }
    
    func setupBackButton(withType type: ButtonType) {
        switch type {
        case .white:
            setupBackButton(withType: .white, withAction: #selector(UIViewController.backButtonTapped))
            break
        case .blue:
            setupBackButton(withType: .blue, withAction: #selector(UIViewController.backButtonTapped))
            break
        }
    }
    
    func setupBackButton(withAction action: Selector) {
        setupBackButton(withType: .blue, withAction: action)
    }
    
    func setupBackButton(withType type: ButtonType, withAction action: Selector) {
        let backButton = backButtonItem(withType: type, withAction: action)
        navigationItem.leftBarButtonItem = backButton
        navigationItem.hidesBackButton = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-300, 0), for: .default)
    }
    
    private func backButtonItem(withType type: ButtonType, withAction action: Selector) -> UIBarButtonItem {
        var imageName: String!
        switch type {
        case .white:
            imageName = "back_button_white"
            break
        case .blue:
            imageName = "left_bar_button_back"
            break
        }
        let image = UIImage.init(named: imageName)?.withRenderingMode(.alwaysOriginal)
        let backButton = UIBarButtonItem.init(image: image, style: .plain, target: self, action: action)
        backButton.accessibilityIdentifier = imageName
        return backButton
    }
    
    func hideBackButton() {
        navigationItem.leftBarButtonItem = nil
    }
    
    private func isFirstViewControllerVisbibleViewController() -> Bool {
        return navigationController?.viewControllers.first == navigationController!.visibleViewController;
    }
    
    @objc private func backButtonTapped() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //    MARK:- right bar button
    
    func setupCloseButton() {
        setupCloseButton(withType: .blue, withAction: #selector(UIViewController.closeTapped))
    }
    
    func setupCloseButton(withType type: ButtonType) {
        switch type {
        case .white:
            setupCloseButton(withType: .white, withAction: #selector(UIViewController.closeTapped))
            break
        case .blue:
            setupCloseButton(withType: .blue, withAction: #selector(UIViewController.closeTapped))
            break
        }
    }
    
    func setupCloseButton(withAction action: Selector) {
        setupCloseButton(withType: .blue, withAction: action)
    }
    
    func setupCloseButton(withType type: ButtonType, withAction action: Selector) {
        let closeButton = closeButtonItem(withType: type, withAction: action)
        navigationItem.rightBarButtonItem = closeButton
        navigationItem.hidesBackButton = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-300, 0), for: .default)
    }
    
    private func closeButtonItem(withType type: ButtonType, withAction action: Selector) -> UIBarButtonItem {
        var image = UIImage(named: "close_blue_icon")?.withRenderingMode(.alwaysTemplate)
        switch type {
        case .white:
            image = image?.maskWithColor(color: AppManager.theme.whiteColor())
        case .blue:
            image = image?.maskWithColor(color: AppManager.theme.themeColor())
        }
        let closeButton = UIBarButtonItem(image: image, style: .plain, target: self, action: action)
        closeButton.accessibilityIdentifier = "close_blue_icon"
        return closeButton
    }
    
    func hideCloseButton() {
        navigationItem.rightBarButtonItem = nil
    }
    
    @objc private func closeTapped() {
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
}
