//
//  SpinnerWrapper.swift
//  TWGiOS
//
//  Created by Samiksha on 04/05/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation
import SVProgressHUD

class SpinnerWrapper {
    
    static func showSpinner() {
        if (!Thread.isMainThread) {
            if !SVProgressHUD.isVisible() {
                SVProgressHUD.show()
            }
        } else {
            SVProgressHUD.show()
        }
    }
    
    static func hideSpinnerView() {
        SVProgressHUD.dismiss()
    }
}
