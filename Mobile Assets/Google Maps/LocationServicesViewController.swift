//
//  LocationServicesViewController.swift
//  TWGiOS
//
//  Created by Arpit Pittie on 24/07/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import UIKit
import GoogleMaps

protocol LocationServicesProtocol {
    func confirmCoordinates(address: Address)
}

class LocationServicesViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var specifyCoordinatesLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var confirmationButton: ButtonNonFilled!
    fileprivate var reverseGeocodeCompletionHandler: ((_ success: Bool, _ address: GMSAddress?) -> ())?
    var delegate: LocationServicesProtocol?
    var address: Address?
    var vendorProfileService: VendorProfileService?
    var vendor: Vendor?
    var isOnboardingLaunch: Bool?
    
    class func fromStoryboard() -> LocationServicesViewController {
        let locationServicesViewController = UIStoryboard(name: "Common", bundle: nil).instantiateViewController(withIdentifier: String(describing: LocationServicesViewController.self)) as! LocationServicesViewController
        return locationServicesViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .denied:
                locationServicesDenied()
            case .authorizedWhenInUse:
                if address != nil {
                    getGPSCoordinates()
                } else {
                    MapsServiceManager.shared.mapsLocationManager().startUpdatingLocation()
                }
            default: break
            }
        }
    }
    
    // MARK: Setup methods
    
    func setup() {
        addressView.isHidden = true
        setupAppearance()
        setupService()
        setupAddress()
        setupMapsServices()
        setupMapView()
        setupNavigationBar()
        reverseGeocodeCompletionHandler = {[weak self] (success, mapAddress) in
            if mapAddress != nil {
                if self?.address == nil {
                    self?.address = Address(street: mapAddress?.thoroughfare, city: mapAddress?.locality, state: mapAddress?.administrativeArea, country: mapAddress?.country, postalCode: mapAddress?.postalCode, latitude: Double((mapAddress?.coordinate.latitude)!), longitude:
                        Double((mapAddress?.coordinate.longitude)!))
                } else {
                    self?.address?.latitude = Double((mapAddress?.coordinate.latitude)!)
                    self?.address?.longitude = Double((mapAddress?.coordinate.longitude)!)
                }
            }
        }
    }
    
    func setupAppearance() {
        addressLabel.textColor = AppManager.theme.charcoalGrayColor()
        specifyCoordinatesLabel.textColor = AppManager.theme.charcoalGrayColor()
        
        addressLabel.font = AppManager.theme.avenirMediumFont(ofSize: 14)
        specifyCoordinatesLabel.font = AppManager.theme.avenirHeavyFont(ofSize: 14)
    }
    
    func setupService() {
        if vendorProfileService == nil {
            vendorProfileService = VendorProfileService(salesforceManager: SalesforceManager.shared)
        }
    }
    
    func setupAddress() {
        if vendor != nil {
            self.address = vendor?.branchAddress
        }
    }
    
    func setupMapsServices() {
        MapsServiceManager.shared.locationManagerDelegate = self
        MapsServiceManager.shared.mapsMarker().map = mapView
        MapsServiceManager.shared.mapsMarker().icon = UIImage(named: "maps_marker")
        MapsServiceManager.shared.mapsLocationManager().requestWhenInUseAuthorization()
    }
    
    func setupMapView() {
        mapView.delegate = self
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
        mapView.animate(toZoom: MapsServiceManager.shared.zoomLevel)
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = true
        if !isOnboardingLaunch! {
            setupBackButton(withType: .blue, withAction: #selector(dismissMe))
        }
    }
    
    func getGPSCoordinates() {
        let localAddress = ArrayToStringConverter.convertStringArrayToString(stringItems: (address?.address)!)
        if localAddress == nil {
            MapsServiceManager.shared.mapsLocationManager().startUpdatingLocation()
            return
        }
        addressLabel.text = localAddress
        
        SpinnerWrapper.showSpinner()
        
        // Mark: Method 1 for geocode
        
        //        MapsServiceManager.shared.mapsAutocompleteFetcher()?.sourceTextHasChanged(localAddress)
        //        MapsServiceManager.shared.autoCompleteSuccessAction = {[unowned self] (predictions) in
        //            if predictions.count != 0 {
        //                MapsServiceManager.shared.mapsPlacesClient().lookUpPlaceID(predictions[0].placeID!) {[unowned self] (result, error) in
        //                    SpinnerWrapper.hideSpinnerView()
        //                    if result?.coordinate != nil {
        //                        DispatchQueue.main.async {
        //                            MapsServiceManager.shared.setMarker(atPosition: (result?.coordinate)!)
        //                            self.mapView.animate(toLocation: (result?.coordinate)!)
        //                        }
        //                    }
        //                }
        //            } else {
        //                self.showAlert(withTitle: nil, message: LocalizableKeys.invalidAddressErrorMsg.localizedString, postDismisshandler: {
        //                    MapsServiceManager.shared.mapsLocationManager().startUpdatingLocation()
        //                })
        //                SpinnerWrapper.hideSpinnerView()
        //            }
        //        }
        //        MapsServiceManager.shared.autoCompleteFailedAction = {(error) in
        //            self.showAlert(withTitle: nil, message: LocalizableKeys.invalidAddressErrorMsg.localizedString, postDismisshandler: {
        //                MapsServiceManager.shared.mapsLocationManager().startUpdatingLocation()
        //            })
        //            SpinnerWrapper.hideSpinnerView()
        //        }
        
        // Mark: Method 2 for geocode
        
        MapsServiceManager.shared.geocodeAddress(address: localAddress!) {[unowned self] (success, coordinate) in
            SpinnerWrapper.hideSpinnerView()
            if coordinate != nil {
                DispatchQueue.main.async {
                    MapsServiceManager.shared.setMarker(atPosition: coordinate!)
                    self.mapView.animate(toLocation: coordinate!)
                    self.addressLabel.text = localAddress
                }
            } else {
                self.showAlert(withTitle: nil, message: LocalizableKeys.invalidAddressErrorMsg.localizedString, postDismisshandler: {
                    MapsServiceManager.shared.mapsLocationManager().startUpdatingLocation()
                })
            }
        }
    }
    
    func locationServicesDenied() {
        showAlert(withTitle: nil, message: LocalizableKeys.locationServicesUsageMsg.localizedString, cancelButtonTitle: LocalizableKeys.cancelButtonTitle.localizedString, otherButtonTitle: LocalizableKeys.goToSettingButtonTitle.localizedString, cancelButtonPostHandler: {[unowned self] in
            self.navigationController?.popViewController(animated: true)
            }, otherButtonPostHandler: {
                UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
    }
    
    // MARK: Action methods
    
    @IBAction func confirmCoordinatesAction(_ sender: UIButton) {
        if isOnboardingLaunch! {
            vendor?.branchAddress = address
            updateVendorProfile()
        } else {
            delegate?.confirmCoordinates(address: address!)
            goBackToParentVC()
        }
    }
    
    @objc func dismissMe() {
        navigationController?.popViewController(animated: true)
    }
    
    func goBackToParentVC() {
        let allViewControllers: NSMutableArray = NSMutableArray.init(array:(self.navigationController?.viewControllers)!)
        for aViewController in allViewControllers {
            if aViewController is VendorEditProfileViewController {
                _ = self.navigationController?.popToViewController(aViewController as! VendorEditProfileViewController, animated: true)
            } else if aViewController is NewServiceRequestViewController {
                _ = self.navigationController?.popToViewController(aViewController as! NewServiceRequestViewController, animated: true)
            } else if aViewController is PlaceBidViewController {
                _ = self.navigationController?.popToViewController(aViewController as! PlaceBidViewController, animated: true)
            }
        }
    }
    
    //    MARK: web service calls
    
    func updateVendorProfile() {
        if vendor != nil {
            SpinnerWrapper.showSpinner()
            vendorProfileService?.updateVendorProfile(vendor!, { [weak self] (vendorProfileResponse, error) in
                SpinnerWrapper.hideSpinnerView()
                if let nsError = error as NSError? {
                    self?.showError(nsError)
                } else if vendorProfileResponse?.vendor != nil {
                    let vendorOnboardingSeenKey = kVendorOnboardingSeen + SalesforceManager.shared.getCurrentUserId()
                    if (UserDefaultsWrapper.shared.retrieveValue(forKey: vendorOnboardingSeenKey) == nil) {
                        UserDefaultsWrapper.shared.save(value: true, forKey: vendorOnboardingSeenKey)
                    }
                    RootViewControllerFactory.configureRootViewController(forType: .applicationVendor, window: (self?.view.window!)!)
                }
            })
        } else {
            showAlert(withTitle: nil, message: LocalizableKeys.updateProfileErrorMsg.localizedString)
        }
    }
}

//  MARK: MapsLocationManagerDelegate methods

extension LocationServicesViewController: MapsLocationManagerDelegate {
    func mapsLocationManager(didFailWithError error: Error) {
        MapsServiceManager.shared.mapsLocationManager().stopUpdatingLocation()
    }
    
    func mapsLocationManager(didUpdateLocation location: CLLocation) {
        let camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: MapsServiceManager.shared.zoomLevel)
        
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
        MapsServiceManager.shared.setMarker(atPosition: location.coordinate)
        if address == nil {
            address = Address(street: nil, city: nil, state: nil, country: nil, postalCode: nil, latitude: Double(location.coordinate.latitude), longitude: Double(location.coordinate.longitude))
        } else {
            self.address?.latitude = Double(location.coordinate.latitude)
            self.address?.longitude = Double(location.coordinate.longitude)
        }
    }
    
    func mapsLocationManager(didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .denied:
            navigationController?.popViewController(animated: true)
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            if address != nil {
                getGPSCoordinates()
            } else {
                MapsServiceManager.shared.mapsLocationManager().startUpdatingLocation()
            }
        default: break
        }
    }
}

//  MARK: GMSMapViewDelegate methods

extension LocationServicesViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        MapsServiceManager.shared.setMarker(atPosition: position.target)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        mapView.animate(to: position)
        MapsServiceManager.shared.reverseGeocodeCoordinate(coordinate: position.target, reverseGeocodeCompletionHandler)
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            guard mapView.myLocation != nil else {
                return false
            }
            MapsServiceManager.shared.setMarker(atPosition: mapView.myLocation!.coordinate)
            mapView.animate(toLocation: mapView.myLocation!.coordinate)
            MapsServiceManager.shared.reverseGeocodeCoordinate(coordinate: mapView.myLocation!.coordinate, reverseGeocodeCompletionHandler)
        }
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        MapsServiceManager.shared.setMarker(atPosition: coordinate)
        MapsServiceManager.shared.reverseGeocodeCoordinate(coordinate: coordinate, reverseGeocodeCompletionHandler)
    }
}
