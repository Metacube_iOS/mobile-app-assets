//
//  Address+Maps.swift
//  TWGiOS
//
//  Created by Arpit Pittie on 29/09/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation
import MapKit

extension Address {
    func openInMaps() {
        guard latitude != nil, longitude != nil else {
            return
        }
        let coordinates = CLLocationCoordinate2DMake(latitude!, longitude!)
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = ArrayToStringConverter.convertStringArrayToString(stringItems: address)
        mapItem.openInMaps(launchOptions: nil)
    }
}
