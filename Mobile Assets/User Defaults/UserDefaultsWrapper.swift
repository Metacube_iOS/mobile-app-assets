//
//  TWGUserDefaults.swift
//  TWGiOS
//
//  Created by Arpit Pittie on 2/20/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

let kUserProfileSeen = "userProfileSeenBy-"
let kProductOnboardingSeen = "productOnboardingSeenBy-"
let kUserType = "userType"
let kVendorOnboardingSeen = "vendorOnboardingSeenBy-"
let kProfileOnboardingSeen = "profileOnboardingSeenBy-"
let kCustomerConactOnboardingSeen = "customerContactOnboardingSeenBy-"
let kAddressOnboardingSeen = "addressOnboardingSeenBy-"
let kFeedbackOnboardingSeen = "feedbackOnboardingSeenBy-"

class UserDefaultsWrapper {
    
    static let shared = UserDefaultsWrapper()
    private let userData = UserDefaults.standard
    
    func save(value: Any?, forKey key: String) {
        userData.set(value, forKey: key)
        userData.synchronize()
    }
    
    func retrieveValue(forKey key: String) -> Any? {
        return userData.object(forKey: key)
    }
    
    func removeObject(forKey key: String) {
        userData.removeObject(forKey: key)
    }
}
