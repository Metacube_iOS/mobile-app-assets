//
//  UIViewController+Alert.swift
//  TWGiOS
//
//  Created by Arpit Pittie on 3/16/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showError(_ error: NSError) {
        if error.getTitle() == nil {
            showAlert(withTitle: error.getMessage(), message: nil)
        } else {
            showAlert(withTitle: error.getTitle(), message: error.getMessage())
        }
    }
    
    func showAlert(withTitle title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: LocalizableKeys.okButtonTitle.localizedString, style: .cancel)
        alert.addAction(action)
        present(alert, animated: true)
        
    }
    
    func showAlert(withTitle title: String?, message: String?, postDismisshandler handler: @escaping ()->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction.init(title: LocalizableKeys.okButtonTitle.localizedString, style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            handler()
        }
        alert.addAction(action)
        present(alert, animated: true)
    }
    
    func showAlert(withTitle title: String?, message: String?, cancelButtonTitle: String, otherButtonTitle: String, cancelButtonPostHandler cancelHandler: (()->())?, otherButtonPostHandler otherHandler: @escaping ()->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action1 = UIAlertAction.init(title: cancelButtonTitle, style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
            if cancelHandler != nil {
                cancelHandler!()
            }
        }
        alert.addAction(action1)
        
        let action2 = UIAlertAction.init(title: otherButtonTitle, style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            otherHandler()
        }
        alert.addAction(action2)
        present(alert, animated: true)
    }
    
    func showLogoutAlert(withTitle title: String?, message: String?, cancelButtonTitle: String, logoutButtonTitle: String, cancelButtonPostHandler cancelHandler: (()->())?, logoutButtonPostHandler logoutHandler: @escaping ()->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: cancelButtonTitle, style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
            if cancelHandler != nil {
                cancelHandler!()
            }
        }
        alert.addAction(action1)
        
        let action2 = UIAlertAction(title: logoutButtonTitle, style: .destructive) { (action) in
            alert.dismiss(animated: true, completion: nil)
            logoutHandler()
        }
        alert.addAction(action2)
        present(alert, animated: true)
    }
}
