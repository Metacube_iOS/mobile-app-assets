//
//  DeepLinkDispatcher.swift
//  TWGiOS
//
//  Created by Arpit Pittie on 31/08/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

class DeepLinkDispatcher {
    
    private var deeplinkItem: DeeplinkItem
    
    init(item: DeeplinkItem) {
        deeplinkItem = item
    }
    
    func openDeepLink(forItem item: DeeplinkItem) -> Bool {
        guard let window = UIApplication.shared.keyWindow else {
            return false
        }
        
        switch item.type {
        case .login:
            let onBoardingPageViewController = OnBoardingPageViewController.fromStoryboard()
            onBoardingPageViewController.jumpToLogonScreen = true
            RootViewControllerFactory.configureRootViewController(forType: .login, window: window)
            window.rootViewController?.present(onBoardingPageViewController, animated: true, completion: nil)
            return true
        case .vendorProfile:
            let homeViewController = VendorTabBarViewController.fromStoryboard()
            homeViewController.pageIndexToLoad = 3
            RootViewControllerFactory.setRootViewController(homeViewController, toWindow: window)
            return true
        case .openActiveRequestsCustomer:
            let serviceRequestTabViewController = ServiceRequestTabViewController.fromStoryboard()
            let navigationViewController = CustomNavigationController(rootViewController: serviceRequestTabViewController)
            navigationViewController.lightStatusBar = true
            
            let leftMenuController = UIStoryboard.init(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
            
            leftMenuController.homeViewController = navigationViewController
            
            let slideMenuViewController = SideMenuWrapper(mainViewController: navigationViewController, leftMenuViewController: leftMenuController)
            
            RootViewControllerFactory.setRootViewController(slideMenuViewController, toWindow: window)
            return true
        case .openActiveJobsVendor:
            RootViewControllerFactory.configureRootViewController(forType: .applicationVendor, window: window)
            return true
        case .openAcceptedJobsVendor:
            let homeViewController = VendorTabBarViewController.fromStoryboard()
            homeViewController.pageIndexToLoad = 1
            RootViewControllerFactory.setRootViewController(homeViewController, toWindow: window)
            return true
        case .openCompletedJobsCustomer:
            let serviceRequestTabViewController = ServiceRequestTabViewController.fromStoryboard()
            serviceRequestTabViewController.pageIndexToLoad = 2
            
            let navigationViewController = CustomNavigationController(rootViewController: serviceRequestTabViewController)
            navigationViewController.lightStatusBar = true
            
            let leftMenuController = UIStoryboard.init(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
            
            leftMenuController.homeViewController = navigationViewController
            
            let slideMenuViewController = SideMenuWrapper(mainViewController: navigationViewController, leftMenuViewController: leftMenuController)
            
            RootViewControllerFactory.setRootViewController(slideMenuViewController, toWindow: window)
            return true
        case .openCompletedJobsVendor:
            return true
        }
    }
}
