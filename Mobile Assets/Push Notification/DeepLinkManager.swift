//
//  DeepLinkManager.swift
//  TWGiOS
//
//  Created by Arpit Pittie on 31/08/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

enum DeepLinkType: String {
    case login = "openLoginScreen"
    case vendorProfile = "openVendorProfile"
    case openActiveJobsVendor = "openActiveJobsVendor"
    case openAcceptedJobsVendor = "openAcceptedJobsVendor"
    case openActiveRequestsCustomer = "openActiveRequestsCustomer"
    case openCompletedJobsCustomer = "openCompletedJobsCustomer"
    case openCompletedJobsVendor = "openCompletedJobsVendor"
}

struct DeeplinkItem {
    var type: DeepLinkType
    var id: String?
}

class DeepLinkManager {
    static let shared = DeepLinkManager()
    
    private init() {}
    
    func handleURL(_ url: URL) -> Bool {
        guard let host = url.host else {
            return false
        }
        
        let type = DeepLinkType(rawValue: host)
        if type != nil {
            let deeplinkItem = DeeplinkItem.init(type: type!, id: nil)
            let dispatcher = DeepLinkDispatcher.init(item: deeplinkItem)
            return dispatcher.openDeepLink(forItem: deeplinkItem)
        }
        return true
    }
}
