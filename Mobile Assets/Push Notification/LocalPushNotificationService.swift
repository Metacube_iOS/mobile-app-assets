//
//  LocalPushNotificationService.swift
//  TWGiOS
//
//  Created by Ankita Porwal on 06/11/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation
import UserNotifications

class LocalPushNotificationService: NSObject {
    
    override init() {
        super.init()
        UNUserNotificationCenter.current().delegate = self
    }
    
    func canShowLocalNotification(_ completion:@escaping ((_ authorized: Bool)->())) {
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { (settings) in
            let result = settings.authorizationStatus == .authorized
            completion(result)
        }
    }
    
    func triggerNotification(_ notification: NotificationItem) {
        canShowLocalNotification {[unowned self] (allowed) in
            if allowed && !DeviceKitWrapper.shared.isDeviceSimulator() {
                self.showNotification(notification)
            } else {
                print("Notification is not allowed by user")
            }
        }
    }
    
    private func showNotification(_ notification: NotificationItem) {
        let content = UNMutableNotificationContent()
        content.body = notification.title!
        content.sound = UNNotificationSound.default()
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.5, repeats: false)
        let request = UNNotificationRequest(identifier:notification.type.rawValue, content: content, trigger: trigger)
        
        
        UNUserNotificationCenter.current().add(request){(error) in
            if (error != nil) {
                print(error?.localizedDescription ?? "Unknown error occured on notification display")
            }
        }
    }
    
    deinit {
        UNUserNotificationCenter.current().delegate = nil
    }
}

extension LocalPushNotificationService: UNUserNotificationCenterDelegate{
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Tapped in notification")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Notification being triggered")
        completionHandler( [.alert,.sound,.badge])
    }
}
