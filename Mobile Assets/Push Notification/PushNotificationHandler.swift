//
//  PushNotificationHandler.swift
//  TWGiOS
//
//  Created by Samiksha on 12/01/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class PushNotificationHandler: NSObject {
    var notificationItem: PushNotificationItem!
    var deeplinkItem: DeeplinkItem!
    
    func shouldHandleNotification(notificationItem: PushNotificationItem) -> Bool {
        return SalesforceManager.shared.isUserActive()
    }
    
    func handleNotificationWith(appState: UIApplicationState?, userInfo: [AnyHashable : Any]){
        let payloadRecieved = userInfo as! Dictionary<String, Any>
        
        var notificationTitle = ""
        if let aps = payloadRecieved["aps"] as? [String : Any] {
            if let alert = aps["alert"] as? [String : Any] {
                if let message = alert["message"] as? String {
                    notificationTitle = message
                }
            } else if let alert = aps["alert"] as? String {
                notificationTitle = alert
            }
        }
        let customPayload = JSON(payloadRecieved["custom-payload"] as Any).string
        
        if let dataFromString = customPayload?.data(using: .utf8, allowLossyConversion: false) {
            let json = JSON(data: dataFromString)
            notificationItem = PushNotificationItem.init(json: json)
            notificationItem.title = notificationTitle
            
            print("NOTIFICATION RECEIVED")
            print(notificationItem.title ?? "")
            print(notificationItem.notificationType)
        }
        
        guard self.shouldHandleNotification(notificationItem: notificationItem) else {
            return
        }
        
        if ( appState == .inactive || appState == .background) {
            setupDeeplinkDispatcher(withNotification: notificationItem)
        } else if appState == .active {
            setupNotificationDispatcher(withNotification: notificationItem)
        }
    }
    
    fileprivate func setupDeeplinkDispatcher(withNotification notification: PushNotificationItem) {
        if notificationItem != nil {
            switch notificationItem.notificationType! {
            case .vendorProfileChangeRequestApproved:
                fallthrough
            case .vendorProfileChangeRequestDeclined:
                deeplinkItem = DeeplinkItem.init(type: .vendorProfile, id: nil)
            case .requestCancelled:
                fallthrough
            case .requestExpired:
                fallthrough
            case .requestExtended:
                fallthrough
            case .bidEdited:
                fallthrough
            case .bidCancelled:
                fallthrough
            case .bidReceived:
                deeplinkItem = DeeplinkItem.init(type: .openActiveRequestsCustomer, id: notificationItem.notificationBidId)
            case .newJobReceived:
                deeplinkItem = DeeplinkItem.init(type: .openActiveJobsVendor, id: notificationItem.notificationJobId)
            case .bidAccepted:
                deeplinkItem = DeeplinkItem.init(type: .openAcceptedJobsVendor, id: notificationItem.notificationBidId)
            case .jobCompletedByVendor:
                deeplinkItem = DeeplinkItem.init(type: .openCompletedJobsCustomer, id: notificationItem.notificationJobId)
            case .jobCompletedByCustomer:
                deeplinkItem = DeeplinkItem.init(type: .openCompletedJobsVendor, id: notificationItem.notificationJobId)
            case .unknown:
                break
            }
            
            let dispatcher = DeepLinkDispatcher.init(item: deeplinkItem)
            _ = dispatcher.openDeepLink(forItem: deeplinkItem)
        }
    }
    
    fileprivate func setupNotificationDispatcher(withNotification notification: PushNotificationItem) {
        if notificationItem != nil {
            switch notificationItem.notificationType! {
            case .vendorProfileChangeRequestApproved:
                let event = VendorProfileUpdateEvent()
                event.status = .approved
                ListenerDispatcher.shared.fire(event: event)
            case .vendorProfileChangeRequestDeclined:
                let event = VendorProfileUpdateEvent()
                event.status = .declined
                ListenerDispatcher.shared.fire(event: event)
            case .requestExpired:
                let event = RequestExpiredEvent()
                event.requestId = notificationItem.notificationRequestId
                ListenerDispatcher.shared.fire(event: event)
            case .requestExtended:
                let event = RequestExtendedEvent()
                event.requestId = notificationItem.notificationRequestId
                ListenerDispatcher.shared.fire(event: event)
            case .requestCancelled:
                let event = RequestCancelledEvent()
                event.requestId = notificationItem.notificationRequestId
                ListenerDispatcher.shared.fire(event: event)
            case .newJobReceived:
                let event = NewJobEvent()
                event.requestId = notificationItem.notificationRequestId
                ListenerDispatcher.shared.fire(event: event)
            case .bidEdited:
                let event =  BidUpdateEvent()
                event.bidId = notificationItem.notificationBidId
                ListenerDispatcher.shared.fire(event: event)
            case .bidReceived:
                let event =  BidReceivedEvent()
                event.bidId = notificationItem.notificationBidId
                ListenerDispatcher.shared.fire(event: event)
            case .bidCancelled:
                let event = CancelBidEvent()
                event.bidId = notificationItem.notificationBidId
                ListenerDispatcher.shared.fire(event: event)
            case .bidAccepted:
                let event =  BidAcceptedEvent()
                event.requestId = notificationItem.notificationJobId
                ListenerDispatcher.shared.fire(event: event)
            case .jobCompletedByCustomer:
                break
            case .jobCompletedByVendor:
                break
            case .unknown:
                break
            }
        }
    }
}
