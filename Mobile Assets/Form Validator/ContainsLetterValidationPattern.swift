//
//  ContainsLetterValidationPattern.swift
//  TWGiOS
//
//  Created by Samiksha on 07/07/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//


import Foundation
import Validator

public struct ContainsLetterValidationPattern: ValidationPattern {
    
    public init() {
    }
    
    public var pattern: String {
        return "[A-Za-z ._']*"
    }
}
