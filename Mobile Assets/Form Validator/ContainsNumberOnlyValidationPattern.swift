//
//  ContainsNumberOnlyValidationPattern.swift
//  TWGiOS
//
//  Created by Arpit Pittie on 26/07/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation
import Validator

public struct ContainsNumberOnlyValidationPattern: ValidationPattern {
    
    public init() {
    }
    
    public var pattern: String {
        return "\\d*"
    }
}
