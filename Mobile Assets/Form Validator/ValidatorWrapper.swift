//
//  ValidatorWrapper.swift
//  TWGiOS
//
//  Created by Samiksha on 28/04/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import UIKit
import Validator

class ValidatorWrapper {
    
    static let shared = ValidatorWrapper()
    
    func isValidEmail(emailString: String) -> Bool {
        let emailRule = ValidationRulePattern(pattern: EmailValidationPattern.standard, error: ErrorBuilder(withTitle: nil, message: LocalizableKeys.invalidEmailErrorTitle.localizedString).build())
        let validationRuleSet = ValidationRuleSet.init(rules: [emailRule])
        let result = emailString.validate(rules: validationRuleSet)
        if (result.isValid) {
            return true
        } else {
            return false
        }
    }
    
    func isValidTextForMinLength(text: String, minimumLength: Int) -> Bool {
        let minLengthRule = ValidationRuleLength(min: minimumLength, error: ErrorBuilder(withTitle: nil, message: LocalizableKeys.blankFieldErrorMsg.localizedString).build())
        let validationRuleSet = ValidationRuleSet.init(rules: [minLengthRule])
        let result = text.validate(rules: validationRuleSet)
        if (result.isValid) {
            return true
        } else {
            return false
        }
    }
    
    func isValidTextForMaxLength(text: String, maximumLength: Int) -> Bool {
        let maxLengthRule = ValidationRuleLength(max: maximumLength, error: ErrorBuilder(withTitle: nil, message: LocalizableKeys.fieldLengthErrorMsg.localizedString).build())
        let validationRuleSet = ValidationRuleSet.init(rules: [maxLengthRule])
        let result = text.validate(rules: validationRuleSet)
        if (result.isValid) {
            return true
        } else {
            return false
        }
    }
    
    func containsAlphabetsValidation(text: String) -> Bool {
        let lettersRulePattern = ContainsLetterValidationPattern()
        let lettersRule = ValidationRulePattern(pattern: lettersRulePattern, error: ErrorBuilder(withTitle: nil, message: LocalizableKeys.specialCharacterAndNumberErrorMsg.localizedString).build())
        let validationRuleSet = ValidationRuleSet.init(rules: [lettersRule])
        let result = text.validate(rules: validationRuleSet)
        if (result.isValid) {
            return true
        } else {
            return false
        }
    }
    
    func containNumbersOnly(text: String) -> Bool {
        let numberRule = ValidationRulePattern(pattern: ContainsNumberOnlyValidationPattern(), error: ErrorBuilder(withTitle: nil, message: LocalizableKeys.onlyNumericCharAllowedErrorMsg.localizedString).build())
        let validationRuleSet = ValidationRuleSet.init(rules: [numberRule])
        let result = text.validate(rules: validationRuleSet)
        if (result.isValid) {
            return true
        } else {
            return false
        }
    }
}
