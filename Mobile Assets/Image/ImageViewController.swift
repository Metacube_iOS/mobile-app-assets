//
//  ImageViewController.swift
//  TWGiOS
//
//  Created by Arpit Pittie on 08/11/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import UIKit
import ImageSlideshow

class ImageViewController: UIViewController {
    
    @IBOutlet weak var imageSlider: ImageSlideshow!
    var photoURLs: [String]?
    var initialPage = 0
    override var prefersStatusBarHidden: Bool {
        return navigationController?.isNavigationBarHidden ?? false
    }
    
    class func fromStoryboard() -> ImageViewController {
        let imageViewController = UIStoryboard (name: "Common", bundle: nil).instantiateViewController(withIdentifier: "\(ImageViewController.self)") as! ImageViewController
        return imageViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: Setup methods
    
    func setupView() {
        setupNavigationBar()
        fetchImageSource()
    }
    
    func setupNavigationBar() {
        setupCloseButton()
        navigationController?.setNavigationBarAppearance(toType: .gradient)
        navigationController?.hidesBarsOnTap = true
    }
    
    func fetchImageSource() {
        guard  (photoURLs != nil) else {
            return
        }
        
        var kingfisherSource: [InputSource] = [InputSource]()
        for URL in photoURLs! {
            if URL.isEmpty == false {
                kingfisherSource.append(KingfisherSource(urlString:URL)!)
            }
        }
        
        imageSlider.currentPageChanged = {[unowned self] (page) in
            self.title = "\(page+1)/\((self.photoURLs?.count)!)"
        }
        imageSlider.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
        imageSlider.backgroundColor = AppManager.theme.blackColor()
        imageSlider.pageControlPosition = .hidden
        imageSlider.contentScaleMode = UIViewContentMode.scaleAspectFit
        imageSlider.circular = false
        imageSlider.zoomEnabled = true
        
        imageSlider.setImageInputs(kingfisherSource)
        imageSlider.setCurrentPage(initialPage, animated: false)
        title = "\(initialPage+1)/\((self.photoURLs?.count)!)"
    }
}
