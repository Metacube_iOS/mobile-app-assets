//
//  ErrorBuilder.swift
//  TWGiOS
//
//  Created by Ankita Porwal on 30/11/16.
//  Copyright © 2016 the warranty group. All rights reserved.
//

import Foundation

struct ErrorCodes {
    static let errorIsNeverLoggedIn = 667
    static let defaultError = 666
}

class ErrorBuilder: NSObject {
    
    var title: String?
    var message: String?
    var domain: String! = "com.twg.error"
    var code: Int = ErrorCodes.defaultError
    
    init(withTitle title: String?, message: String?) {
        super.init()
        self.title = title
        self.message = message
    }
    
    init(withTitle title: String?, message: String?, code: Int) {
        super.init()
        self.title = title
        self.message = message
        self.code = code
    }
    
    func build() -> NSError {
        return NSError.getError(withMessge: message, title: title, domain: domain, code: code)
    }
}
