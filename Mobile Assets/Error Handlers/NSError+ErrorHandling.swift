//
//  ErrorWrapper.swift
//  TWGiOS
//
//  Created by Ankita Porwal on 30/11/16.
//  Copyright © 2016 the warranty group. All rights reserved.
//

import Foundation

let kErrorTitle = "ErrorTitle";

extension NSError {
    
    class func getError(withMessge message: String!, title: String?, domain: String!, code: Int!) -> NSError {
        var userInfoCopy = Dictionary<String, String>()
        userInfoCopy[NSLocalizedDescriptionKey] = message
        if title != nil {
            userInfoCopy[kErrorTitle] = title
        }
        let error = NSError(domain: domain, code: code, userInfo: userInfoCopy)
        return error
    }
    
    private func isTitlePresent() -> Bool {
        return (userInfo[kErrorTitle] != nil)
    }
    
    private func isMessagePresent() -> Bool {
        return (userInfo[NSLocalizedDescriptionKey] != nil)
    }
    
    func getTitle() -> String? {
        if isTitlePresent() {
            return (userInfo[kErrorTitle] as! String?)!
        }
        return nil
    }
    
    func getMessage() -> String {
        if isMessagePresent() {
            return (userInfo[NSLocalizedDescriptionKey] as! String?)!
        } else {
            return LocalizableKeys.genericErrorMsg.localizedString
        }
    }
}
