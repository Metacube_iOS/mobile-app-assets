//
//  UploadService.swift
//  TWGiOS
//
//  Created by Arpit Pittie on 03/08/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

class UploadService {
    private var salesforceManager: SalesforceManager!
    
    init(salesforceManager manager: SalesforceManager) {
        salesforceManager = manager
    }
    
    func uploadImage(forObjectType objectType: MediaUploadType, objectId: String, image: ImageAsset, _ completionHandler: ((_ success: Bool, _ error: Error?, _ response: String?) -> ())?) {
        salesforceManager.uploadImage(forObjectType: objectType, objectId: objectId, image: image) {(success, error, url) in
            if completionHandler != nil {
                completionHandler!(success, error, url)
            }
        }
    }
    
    func deleteImage(forObjectType objectType: MediaUploadType, objectId: String, url: String, _ completionHandler: ((_ success: Bool, _ error: Error?, _ response: String?) -> ())?) {
        salesforceManager.deleteImage(forObjectType: objectType, objectId: objectId, url: url) {(success, error, response) in
            if completionHandler != nil {
                completionHandler!(success, error, response)
            }
        }
    }
}
