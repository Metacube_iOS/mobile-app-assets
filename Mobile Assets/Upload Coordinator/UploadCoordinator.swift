//
//  UploadCoordinator.swift
//  TWGiOS
//
//  Created by Arpit Pittie on 03/08/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import Foundation

class UploadCoordinator {
    private var salesforceManager: SalesforceManager!
    private var operationQueue = OperationQueue()
    private var uploadService: UploadService!
    
    private var successUploadImages = [ImageAsset]()
    private var failedUploadImages = [ImageAsset]()
    private var newImageURLs = [String]()
    
    typealias UploadCompletionHandler = ((_ success: Bool, _ successImageUploads: [ImageAsset]?, _ failedImageUploads: [ImageAsset]?, _ newImageURLs: [String]?) -> ())
    typealias DeleteCompletionHandler = ((_ success: Bool, _ successImageDelete: [String]?, _ failedImageDelete: [String]?, _ newImageURLs: [String]?) -> ())
    
    init(salesforceManager manager: SalesforceManager, maxConcurrentUploads: Int) {
        salesforceManager = manager
        uploadService = UploadService(salesforceManager: manager)
        operationQueue.name = "Product.multiImageUpload.UploadCoordinator"
        operationQueue.maxConcurrentOperationCount = maxConcurrentUploads
    }
    
    func uploadImages(forProducts products: [Product], _ fileCount: Int, _ completionHandler: UploadCompletionHandler?) {
        for product in products {
            uploadImages(forProduct: product, fileCount, completionHandler)
        }
    }
    
    func uploadImages(forProduct product: Product, _ fileCount: Int, _ completionHandler: UploadCompletionHandler?) {
        for imageAsset in product.imageAssets {
            operationQueue.addOperation { [weak self] in
                self?.uploadService.uploadImage(forObjectType: .product, objectId: product.id!, image: imageAsset) {[weak self] (success, error, response) in
                    if let url = response {
                        self?.newImageURLs.append(url)
                        self?.successUploadImages.append(imageAsset)
                    } else {
                        self?.failedUploadImages.append(imageAsset)
                    }
                    
                    if self?.failedUploadImages.count == fileCount {
                        if completionHandler != nil {
                            completionHandler!(false, self?.successUploadImages, self?.failedUploadImages, self?.newImageURLs)
                        }
                    } else if ((self?.successUploadImages.count)! + (self?.failedUploadImages.count)!) == fileCount {
                        if completionHandler != nil {
                            completionHandler!(true, self?.successUploadImages, self?.failedUploadImages, self?.newImageURLs)
                        }
                    }
                }
            }
        }
    }
    
    //MARK: Service Request 
    
    func uploadImages(forRequest request: JobRequest, _ fileCount: Int, _ completionHandler: UploadCompletionHandler?) {
        for imageAsset in request.imageAssets {
            operationQueue.addOperation { [weak self] in
                self?.uploadService.uploadImage(forObjectType: .serviceRequest, objectId: request.id!, image: imageAsset) {[weak self] (success, error, response) in
                    if let url = response {
                        self?.newImageURLs.append(url)
                        self?.successUploadImages.append(imageAsset)
                    } else {
                        self?.failedUploadImages.append(imageAsset)
                    }
                    
                    if self?.failedUploadImages.count == fileCount {
                        if completionHandler != nil {
                            completionHandler!(false, self?.successUploadImages, self?.failedUploadImages, self?.newImageURLs)
                        }
                    } else if ((self?.successUploadImages.count)! + (self?.failedUploadImages.count)!) == fileCount {
                        if completionHandler != nil {
                            completionHandler!(true, self?.successUploadImages, self?.failedUploadImages, self?.newImageURLs)
                        }
                    }
                }
            }
        }
    }
    
    func uploadImages(forInvoice invoice: Invoice, _ fileCount: Int, _ completionHandler: UploadCompletionHandler?) {
        for imageAsset in invoice.imageAssets {
            operationQueue.addOperation { [weak self] in
                self?.uploadService.uploadImage(forObjectType: .invoice, objectId: (invoice.reportId)!, image: imageAsset) {[weak self] (success, error, response) in
                    if let url = response {
                        self?.newImageURLs.append(url)
                        self?.successUploadImages.append(imageAsset)
                    } else {
                        self?.failedUploadImages.append(imageAsset)
                    }
                    
                    if self?.failedUploadImages.count == fileCount {
                        if completionHandler != nil {
                            completionHandler!(false, self?.successUploadImages, self?.failedUploadImages, self?.newImageURLs)
                        }
                    } else if ((self?.successUploadImages.count)! + (self?.failedUploadImages.count)!) == fileCount {
                        if completionHandler != nil {
                            completionHandler!(true, self?.successUploadImages, self?.failedUploadImages, self?.newImageURLs)
                        }
                    }
                }
            }
        }
    }
    
    func uploadImage(forObjectType objectType: MediaUploadType, objectId: String, image: ImageAsset, _ completionHandler: ((_ success: Bool, _ error: Error?, _ response: String?) -> ())?) {
        uploadService.uploadImage(forObjectType: objectType, objectId: objectId, image: image) { (success, error, response) in
            if completionHandler != nil {
                completionHandler!(success, error, response)
            }
        }
    }
    
    func deleteImage(forObjectType objectType: MediaUploadType, objectId: String, url: String, _ completionHandler: ((_ success: Bool, _ error: Error?) -> ())?) {
        uploadService.deleteImage(forObjectType: objectType, objectId: objectId, url: url) {(success, error, response) in
            if completionHandler != nil {
                completionHandler!(success, error)
            }
        }
    }
}
