//
//  SalesforceRestAPIWrapper
//  TWGiOS
//
//  Created by Ankita Porwal on 11/05/17.
//  Copyright © 2017 the warranty group. All rights reserved.
//

import UIKit
import SalesforceSDKCore

extension SalesforceManager {
    
    func send(_ request: SFRestRequest, complete completionBlock: ((_ success: Bool, _ error: Error?, _ object: Any?) -> ())?) {
        sfRestAPI().send(request, fail: { (error) in
            if completionBlock != nil {
                if let nsError = error as NSError? {
                    switch nsError.code {
                    case NSURLErrorNotConnectedToInternet:
                        let newError = ErrorBuilder.init(withTitle: LocalizableKeys.networkErrorTitle.localizedString,
                                                         message: LocalizableKeys.networkErrorMsg.localizedString).build()
                        completionBlock!(false, newError, nil)
                    case NSURLErrorTimedOut:
                        let newError = ErrorBuilder.init(withTitle: LocalizableKeys.networkErrorTitle.localizedString,
                                                         message: LocalizableKeys.timeoutErrorMsg.localizedString).build()
                        completionBlock!(false, newError, nil)
                    default:
                        if let errors = nsError.userInfo["error"] as? NSArray, errors.count > 0, let topError = errors[0] as? [String:String] {
                            _ = topError["errorCode"]
                            let errorMessage = topError["message"]
                            if errorMessage != nil {
                                let newError = ErrorBuilder.init(withTitle: nil, message: errorMessage).build()
                                completionBlock!(false, newError, nil)
                            }
                        } else {
                            let newError = ErrorBuilder.init(withTitle: nil,
                                                             message: LocalizableKeys.genericErrorTryAgainMsg.localizedString).build()
                            completionBlock!(false, newError, nil)
                        }
                    }
                } else {
                    let newError = ErrorBuilder.init(withTitle: nil,
                                                     message: LocalizableKeys.genericErrorTryAgainMsg.localizedString).build()
                    completionBlock!(false, newError, nil)
                }
            }
        }) { (response) in
            if completionBlock != nil {
                completionBlock!(true, nil, response)
            }
        }
    }
}
